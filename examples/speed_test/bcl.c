//
// Created by why on 2021/5/29.
//
//
// Created by why on 2021/3/4.
//

#include "work_server.h"
#include "runtime.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <nng/nng.h>
#include <dlfcn.h>
#ifndef COUNT
#define COUNT 100000
#endif
#define nDEBUG

/**
 * 异步工作请求
 */
struct work_task {
	int wid;
	enum {INIT, RECV, WORK, DONE, SEND} state;
	nng_aio *aio;
	nng_ctx ctx;
	nng_msg *msg;
	func_ptr_t fp;
	void * param;
	mate_date_s md;
};

struct args{
	int m;
	int n;
};

/*
 * Mate_date related to the return value of function.
 */

static void fatal(const char *func, int rv)
{
	fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
	exit(1);
}

static void
func_wrapper(void * ud) {
	struct work_task * wt_p = ud;
	wt_p->fp(wt_p->param, &wt_p->md);
#ifdef DEBUG
	printf("func_wrapper : task%d work done\n",wt_p->wid);
#endif
	wt_p->state = DONE;
}



static int do_work(struct work_task * work_task)
{
#ifdef DEBUG
	printf("work_task %d start working \n", work_task->wid);
#endif
	brick_new(func_wrapper, (void *)work_task);
}

int main(void)
{
	struct work_task * work_task = malloc(sizeof(*work_task));
	struct args * arg = malloc(sizeof(*arg));
	arg->m = 100;
	arg->n = 999;
	const char* lib_path = "./libwork_ppdi.so";
	const char* func_name = "work";
	void* lib = dlopen(lib_path, RTLD_LAZY);
	func_ptr_t fp = dlsym(lib, func_name);
	work_task->fp = fp;
	work_task->param = arg;
	runtime_init(COUNT);
	printf("start\n");
	for (int i = 0; i < COUNT; ++i) {
		do_work(work_task);
	}
	for (;;) {
		usleep(3600000); // neither pause() nor sleep() portable
	}
}
