cmake_minimum_required(VERSION 3.13)
if(COMMAND cmake_policy)
    cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)
project(fork_test C)
SET(CMAKE_BUILD_TYPE "Debug")
SET(CMAKE_CXX_FLAGS_DEBUG   "$ENV{CXXFLAGS} -O0 -Wall -g -pg -ggdb")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
SET(CMAKE_CXX_FLAGS_RELEASE  "$ENV{CXXFLAGS} -O3 -Wall")

set(CMAKE_C_STANDARD 99)
find_package(nng CONFIG REQUIRED)
find_package(Threads)



set(CMAKE_C_STANDARD 99)

add_executable(load load.c)
target_link_libraries(load dl)

add_executable(fork fork.c)

add_executable(fork-4 fork-4.c)

add_executable(work work.c)
target_link_libraries(work m)

add_executable(baseline baseline.c)
target_link_libraries(baseline m)

add_library(work.so SHARED work.c)
target_link_libraries(work.so m)

add_library(work_ppdi SHARED workload_ppdi.c)
target_link_libraries(work_ppdi m)

add_executable(bcl bcl.c work_server.h runtime.c runtime.h)
target_link_libraries(bcl nng::nng dl ${CMAKE_THREAD_LIBS_INIT})